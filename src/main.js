import Vue from 'vue'
import App from './App.vue'
import moment from 'moment';
import * as vueMoment from 'vue-moment';
import * as momentDurationFormat from 'moment-duration-format';
import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css';
import * as VueGoogleMaps from 'vue2-google-maps'

momentDurationFormat(moment);

Vue.config.productionTip = false
Vue.use(Vuetify)
Vue.use(vueMoment, {moment});
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAoGq3X_mCNoCZBbInX0sGXpWfN9fSIv0o',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
  }
})

new Vue({
  render: h => h(App)
}).$mount('#app')

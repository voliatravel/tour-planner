export default [
    {
        text: 'Hotel',
        icon: 'hotel'
    },
    {
        text: 'Airport',
        icon: 'local_airport'
    },
    {
        text: 'Car',
        icon: 'directions_car'
    }
]